module gitlab.com/replix/azure-instance-metadata

go 1.15

require (
	github.com/go-resty/resty/v2 v2.4.0
	github.com/google/uuid v1.1.5
)
