package imds

import (
	"encoding/json"
	"errors"

	"github.com/go-resty/resty/v2"
)

const azureInstanceMeadataService = "http://169.254.169.254"
const instanceMetadata = "/metadata/instance"

// InstanceMetadata - get instance metadata
func InstanceMetadata() (*Instance, error) {
	client := resty.New().
		SetHostURL(azureInstanceMeadataService)
	response, err := client.
		R().
		SetQueryParams(map[string]string{
			"format":     "json",
			"ap-version": "2020-10-01",
		}).
		SetHeader("Metadata", "True").
		SetHeader("Accept", "application/json").
		Get(instanceMetadata)

	var instanceMetadata *Instance

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, instanceMetadata)
		return instanceMetadata, err
	}
	return nil, errors.New("Failed to fetch Azure Instance Metadata")
}
