package imds

import (
	"github.com/google/uuid"
)

// Instance - Azure instance metadata
type Instance struct {
	Compute Compute `json:"compute"`
	Network Network `json:"network"`
}

// Compute - describe Azure compute element
type Compute struct {
	AzEnvironment              string    `json:"azEnvironment"`
	IsHostCompatibilityLayerVM bool      `json:"isHostCompatibilityLayerVm"`
	LicenseType                string    `json:"licenseType,omitempty"`
	Location                   string    `json:"location"`
	Name                       string    `json:"name"`
	Offer                      string    `json:"offer"`
	OsProfile                  OsProfile `json:"osProfile"`
	OsType                     string    `json:"osType"`
	PlacementGroupID           uuid.UUID `json:"placementGroupId"`
	// plan
	// platformUpdateDomain
	// platformFaultDomain
	// provider
	// publicKeys
	// publisher
	ResourceGroupName string `json:"resourceGroupName"`
	ResourceID        string `json:"resourceId"`
	Sku               string `json:"sku"`
	// securityProfile
	// storageProfile
	Tags string `json:"tags"`
	// tagsList
	Version        string    `json:"version"`
	VMID           uuid.UUID `json:"vmId"`
	VMScaleSetName string    `json:"vmScaleSetName"`
	VMSize         string    `json:"vmSize"`
	Zone           string    `json:"zone"`
}

// OsProfile - well OS Profile
type OsProfile struct {
	AdminUsername                 string `json:"adminUsername"`
	ComputerName                  string `json:"computerName"`
	DisablePasswordAuthentication bool   `json:"disablePasswordAuthentication"`
}

// Network description
type Network struct {
	Interface []Interface `json:"interface"`
}

// Interface description
type Interface struct {
	IPv4       IPv4   `json:"ipv4"`
	IPv6       IPv6   `json:"ipv6"`
	MacAddress string `json:"macAddress"`
}

// IPv4 - IPv4 description
type IPv4 struct {
	IPAddress []IPAddress `json:"ipAddress"`
	Subnet    Subnet      `json:"subnet"`
}

// IPv6 - IPv4 description
type IPv6 struct {
	IPAddress []IPAddress `json:"ipAddress"`
	Subnet    Subnet      `json:"subnet"`
}

// IPAddress - IP address configuration
type IPAddress struct {
	PrivateIPAddress string `json:"privateIpAddress"`
	PublicIPAddress  string `json:"publicIpAddress"`
}

// Subnet description
type Subnet struct {
	Address string `json:"address"`
	Prefix  string `json:"prefix"`
}

/*
{
    "compute": {
        "azEnvironment": "AZUREPUBLICCLOUD",
        "isHostCompatibilityLayerVm": "true",
        "licenseType":  "Windows_Client",
        "location": "westus",
        "name": "examplevmname",
        "offer": "Windows",
        "osProfile": {
            "adminUsername": "admin",
            "computerName": "examplevmname",
            "disablePasswordAuthentication": "true"
        },
        "osType": "linux",
        "placementGroupId": "f67c14ab-e92c-408c-ae2d-da15866ec79a",
        "plan": {
            "name": "planName",
            "product": "planProduct",
            "publisher": "planPublisher"
        },
        "platformFaultDomain": "36",
        "platformUpdateDomain": "42",
        "publicKeys": [{
                "keyData": "ssh-rsa 0",
                "path": "/home/user/.ssh/authorized_keys0"
            },
            {
                "keyData": "ssh-rsa 1",
                "path": "/home/user/.ssh/authorized_keys1"
            }
        ],
        "publisher": "RDFE-Test-Microsoft-Windows-Server-Group",
        "resourceGroupName": "macikgo-test-may-23",
        "resourceId": "/subscriptions/xxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx/resourceGroups/macikgo-test-may-23/providers/Microsoft.Compute/virtualMachines/examplevmname",
        "securityProfile": {
            "secureBootEnabled": "true",
            "virtualTpmEnabled": "false"
        },
        "sku": "Windows-Server-2012-R2-Datacenter",
        "storageProfile": {
            "dataDisks": [{
                "caching": "None",
                "createOption": "Empty",
                "diskSizeGB": "1024",
                "image": {
                    "uri": ""
                },
                "lun": "0",
                "managedDisk": {
                    "id": "/subscriptions/xxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx/resourceGroups/macikgo-test-may-23/providers/Microsoft.Compute/disks/exampledatadiskname",
                    "storageAccountType": "Standard_LRS"
                },
                "name": "exampledatadiskname",
                "vhd": {
                    "uri": ""
                },
                "writeAcceleratorEnabled": "false"
            }],
            "imageReference": {
                "id": "",
                "offer": "UbuntuServer",
                "publisher": "Canonical",
                "sku": "16.04.0-LTS",
                "version": "latest"
            },
            "osDisk": {
                "caching": "ReadWrite",
                "createOption": "FromImage",
                "diskSizeGB": "30",
                "diffDiskSettings": {
                    "option": "Local"
                },
                "encryptionSettings": {
                    "enabled": "false"
                },
                "image": {
                    "uri": ""
                },
                "managedDisk": {
                    "id": "/subscriptions/xxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx/resourceGroups/macikgo-test-may-23/providers/Microsoft.Compute/disks/exampleosdiskname",
                    "storageAccountType": "Standard_LRS"
                },
                "name": "exampleosdiskname",
                "osType": "Linux",
                "vhd": {
                    "uri": ""
                },
                "writeAcceleratorEnabled": "false"
            }
        },
        "subscriptionId": "xxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx",
        "tags": "baz:bash;foo:bar",
        "version": "15.05.22",
        "vmId": "02aab8a4-74ef-476e-8182-f6d2ba4166a6",
        "vmScaleSetName": "crpteste9vflji9",
        "vmSize": "Standard_A3",
        "zone": ""
    },
    "network": {
        "interface": [{
            "ipv4": {
               "ipAddress": [{
                    "privateIpAddress": "10.144.133.132",
                    "publicIpAddress": ""
                }],
                "subnet": [{
                    "address": "10.144.133.128",
                    "prefix": "26"
                }]
            },
            "ipv6": {
                "ipAddress": [
                 ]
            },
            "macAddress": "0011AAFFBB22"
        }]
    }
}
*/
